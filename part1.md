

# Part I : Let's write some hardware

The goal of this part is to familiarize yourself with VHDL and learn how to produce a simple circuit that implements a certain high-level functionality.
We assume that you are familiar with VHDL having followed (and understood) the tutorial from part 0.

For all what follows, we will assume that we want to produce hardware to handle operations on signed 32-bits intergers represented in two's complement.
Note that you should write all the code yourself and not copy from your other classmates.
If you do copy the code from somewhere else (e.g. the VHDL tutorial), always indicate where each code portion has been copied from.


## 1. Single operations

### Arithmetic operations


First, write down the implementation for the addition of two integers.
We will use the semantic of C for signed integer addition where overflow is ignored; i.e. adding two 32 bits integers results in a 32 bit integer.
For instance, adding 1 to the maximum positive number representable `2147483647` (=2^31-1) results in `-2147483648`.

Once you have written the component, write a testbench to test its behaviour and ensure that the correct results are produced for different inputs.

Next, repeat the same process and implement in VHDL the multiplication of two integers.
Here again we will truncate the result to a 32 bit integer.

You may opt to use *generics* to have the flexibility, later, to change the bit width if you wish to.


### Memory operations

Next, we will look at supporting memory operations by producing an on-chip blockram.
We will use a dual port RAM for this purposes and you can follow the [tutorial on designing Random access memory](https://vhdlguide.readthedocs.io/en/latest/vhdl/dex.html#random-access-memory-ram).

Initialize the content of the RAM with some values.
The first answer in this [stackoverflow thread](https://stackoverflow.com/questions/19751148/quartus-initializing-ram) should allow you to initialize the RAM from a file.
Then test your component using a testbench.

## 2. Putting it altogether

Now you are equipped with your basic components, design a circuit that implements the following C-like program:

```C
mem = malloc(4*1024); // allocating 1024 32-bit integers
mem[3] = mem[0] * mem[1] + mem[2];
```

whose corresponding data flow graph is:

![alt text](./dots/dfg1.png "data flow graph")

where the *LD* and *ST* nodes correspond, respectively, to a load from and a store to the block RAM.

To implement this circuit, you will have to instantiate one dual-port RAM, one adder and one multiplier to form the so-called *data-path* where values flow between components through registers, holding temporary results.

You will have to design a finite state machine to *control* the order of operations.
The finite state machine should control when the values are stored in registers (enable signal of the register) and when the "write-enable" *we* signal of the RAM is enabled.
It should have as many states as you need clock cycles to perform all the operations.

First, implement the following sequential schedule:

1. perform `mem[0]` at clock cycle 1
2. `mem[1]` at clock cycle 2
3. perform the multiplication at clock cycle 3
4. perform `mem[2]` at clock cycle 4
5. perform the addition at clock cycle 5
6. finally perform the write into the RAM at clock cycle 6

Make sure to test your circuit by writing a testbench and also simulate it to check that each operations happens in the correct clock cycle.


The astute student will have perhaps observe that it is possible to optimize the schedule and produce the expected result in only 5 clock cycles.
Observe how the multiplication and loading of the RAM at address 2 can actually happen in parallel since these operations do not share any resources.
Keep your data-path (components and their connections) unchanged and simply modify your finite state machine to achieve this new schedule and test your circuit again.
