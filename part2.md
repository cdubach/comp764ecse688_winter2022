# Part II. Program representations

Having manually designed your first hardware to implement a high-level functionality, we will now turn our attention to the compiler side of things.
Our goal is to automate the generation of hardware.
To this end, you will first need to design an intermediate representation of the input high-level program.

## Abastract Syntax Tree (AST)

### AST nodes

The classical approach consists of using an AST (Abstract Syntax Tree) whose nodes are the operations supported by the input language.
In general, this AST would be produced automatically by a parser (that could either be hand-written or generated using a tool).
However, for this course, we will not bother with developing a parser and instead build the AST programmatically.

In order to support the hardware operations seen earlier, we will need at least six concrete AST nodes:

* Addition, Multiplication
* Memory allocation
* Memory Load and Store
* Constant

If you are using an Object Oriented programming language (e.g Java, Scala), the class hierarchy might look something like that:

```Scala
class Expr extends ASTNode;
class Stmt extends ASTNode;
class Mem(size: int) extends ASTNode; // size in byte

class Add(l: Expr, r: Expr) extends Expr;
class Mul(l: Expr, r: Expt) extends Expr;

class Load(mem: Mem, addr: Expr) extends Expr;
class Store(mem: Mem, addr: Expr, val: Expr) extends Stmt;

class Cst(i: int) extends Expr;
```

In the pseudo-code above, the abstract class `Expr` represents expressions (anything that can return a value) and the abstract class `Stmt` represents statements (anything that changes the state of the machine).
What follows the class name are the fields (i.e. children) of the node.
We assume in this example, that the expressions always represent 32 bit integer and that these values are truncated when passed as an address to a load/store operation.
Note that in a real AST, you would also have type information attached to each Expr that would inform you about the data type.
We will assume for now that the only data type we are dealing with is int32.



### Representing a Program

Since we do not wish to write a parser for our input language, we will have to programmatically write our input programs.
For instance, the code seen earlier:
```C
RAM = malloc(1024);
RAM[3] = RAM[0] * RAM[1] + RAM[2]
```
could be constructed programmatically in an Object Oriented programming language as:
```Scala
ram = new Mem(1024);
new Store(
  ram,
  Cst(3),
  new Add(
    new Mul(
	  new Load(ram,Cst(0)),
	  new Load(ram,Cst(1))),
	new Load(ram,Cst(2))));
```

Depending on your choice of programming language, you may be able to greatly simply the code above.
For instance, in Scala, you could use case classes, implicit conversion and the ability to use infix notation.
The above code could be reduced to:
```Scala
val ram = Mem(1024);
Store(ram, 3, Load(mem, 0) * Load(mem, 1) + Load(mem, 2));
```

### Testing out the AST with an interpreter

While not strictly necessary, it might be helpful for debugging purposes to write a small AST interpreter.
This will allow you to test that you are building your tree correctly.

Depending on the language choosen, you can either implement this using a visitor, or use pattern matching if your language supports it (e.g. Scala).

Observe that since the only state is in the memory object, you just need to have a *native* array associated with each memory object (you can store this array in a *Map* collection where the key is the Mem object).
When interpreting a load or store operations, these will either read a value from the array or write a value into it.
Again, we assume here that all the data type processed are 32bit integers.

Given that a program can only interact with memory, you will need to have a way to initialize memory.
You should extend your memory class/object with a field (and constructor argument) to specify what the content of the memory should before the program interpretation starts.


## Control Data Flow Graph

As seen in the lecture, for the purpose of hardware generation (and code generation in software), a compiler usually represents programs at a lower level with a CDFG (Control Data Flow Graph).

### Direct graphs

You should first create some data structures to represent the control flow graph whose nodes will contain a data flow graph.
Note that both type of graphs are directed.
Feel free to use a library or implement your own graph data structures.


### Dataflow Nodes
The nodes of the data flow graph will be:

* Add, Mul
* Load, Store
* Cst

Note that you should not confuse these nodes with the classes that you have designed to represent the AST.
In the AST, an Add object, for instance, has two fields representing its children which are the input to the addition.
In the dataflow graph, the Add node, for instance, does not store any information about the input since this is encoded explicitly in the data flow graph.
The only node that will store information are the Load and Store node which will contain information about which memory they should access.



### From AST to CDFG

Your next task should consists in producing the CDFG from the AST.
You should write a simple pass that traverses recursively the AST and produce a CDFG.
Since, so far, there are no control structure in our AST, the resulting CDFG should contain a single node representing the entire program as a single DFG.


### Visualization

To help you visualize the graphs produced from the AST, we suggest that you implement a simple DOT printer.
DOT is a very simple language used to represent graphs.
Check this [website])(https://renenyffenegger.ch/notes/tools/Graphviz/examples/index) out for examples of graphs produced from an input dot file.
On Linux, if you have a dot file, you can produce a graph easily by using this command:
`dot -Tpdf input.dot -o output.pdf`

A DOT printer can simply be implemented by traversing your directed graph, ensuring each node is only traversed once.

