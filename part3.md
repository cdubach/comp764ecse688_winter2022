# Part III. VHDL generation

Now that you are equipped with an internal representation for your program, you will write a hardware code generator.
As you should know by now, any traditional hardware consists of control logic and datapath.
In our case, the control logic will simply be a finite state machine that determines when values are written into registers, when values are read/written from/to a blockram, and which input to select from the multiplexers present in the datapath.

We will assume that we wish to support circuits composed of adders, multipliers and several RAM blocks.
Furthermore, we will assume for now that the control flow graph only has a single node which contains one data flow graph (i.e. our program does not have any control structures such as if-then-else or loops).

## Scheduling

Your first task, before hardware generation can properly take place, should be to schedule the operations in the data flow graph.
We will assume that we have an infinite number of functional units such as *adders* and *multipliers*.
However, there can only be a single memory access (either load or store) performed per cycle to the same RAM.

At this point, we do not care about optimality but only about correctness.
We recommed that you implement the simple *list scheduling* algorithm seen during class where each node in the graph will be given the same priority (i.e. you do not need to use the ASAP/ALAP schedule to determine mobility which is typically used for the priority function).
As explained in class, you should create one ready list per resource, and only schedule operations that are ready and for which you can find an available resource.
In our case, this will be one list for the adder, one for the multiplier, and one list per memory that you have intantiated (since memory can only be used to either read or write in the same clock cycle, we have a single list per memory, and in terms of resource usage, do not distinguish between a load or store operation).

**Output**: for each node n in the DFG: n -> time

For the example given below, you should check that your schedule indeed matches with the expect time.


## Resource binding

Next, you will have to bind each operation to a specific hardware resource (one of the entity that you will instantiate).
To achive this, simply traverse the schedule produced and for each operation that occurs at the same time, assign a unused resource for that clock cycle.
While we do assume an infinite number of resources, we ask you to reuse as much as possible the resources.
For instance if two adders are used at time 1 and one adder is used at time 2, the total number of adders in your circuits should be 2, since one of the two adders can be reused at clock cycle 1.

You should probably represent a resource as an intance of a Resource class, which should will contain the following information:

* name of VHDL entity
* input/output ports

This resource class in fact simply models an entity that will be instantiated as a component later on.

**Output**: for each node n in the DFG: n -> Resource


## Register Allocation

Next, you will have to establish where each operation should write its result into.
To keep things simple, we will assume that we have an infinite number of registers.
Therefore, you should simply assign one register for each node producing a result (everything except *store* operations).

Here again, you should probably introduce a new class to represent Register.
Note that while at the moment each node produces a result consummed by a single node, there might be a case in the future of a node producing one result consummed by mutiple nodes.
This might require to save the value produced in multiple registers.
Therefore, registers should be associated with the edges in the DFG and not the nodes. 

**Output**: for each edge in the DFG: edge -> Register


## Datapath generation

Once you have established your schedule, have your resources bound and have a register assigned for each operation producing a result, you are ready to produce the datapath.
The main difficulty when producing the datapath consists in connecting the various components together and placing the multiplexers in the right place.

Instead of directly trying to produce a VHDL code representing the datapath, we strongly suggest that you first produce a graph representing a datapath.
The nodes of the graphs should be resources created during the resource binding stages, the registers introduced in the register allocation phase as well as multipixers that you should insert when needed as seen in the lecture example.
Notice how this graph representing the datapath is likely to contain cycles (in contrast to the DFG which is acyclic)


Once you have built your datapath as a graph, it is straight-forward to generate VHDL code for it.
Produce VHLD code that declares one temporary signal per edge in the graph.
You should also declare one temporary signal per register and per load/store resource for the *enable* port, and finally one *selection* signal per multiplexer.
you should remember to store the association between these temporary signals and the resource/multiplexer as this information will be needed when producing the state machine.
Then continue with generating VHDL code that instantiate a component for each node in the graph, using the input/ouput signals attached to the each edge to perform the port mapping.




## Control generation (state machine)

Your final task will consists in producing the finite state machine that controls the datapath.
Your finite state machine will have one state per clock cycle and the machine always moves to the next step after each clock cycle.
You may want to add a final END state where the machine do not perform any operations and simply stays in this state.

To determine how the control signals should be produced, proceed as follows.
Given a state representing time *t*, extract from the schedule the list of nodes that are active.
This will return some nodes from the DFG.

For each node returned, do the following:

* if the node produces a result (everything except stores), set the *enable* signal of the registers (potentially one per child) associated with the node to '1';
* if the node is a memory operation, set the corresponding *enable* signal to '1';
* find the associated resource in the datapath graph, and for each input (e.g. an ADD has two inputs):
    * if the input is a multiplexer in the datapath graph, find in the DFG the node corresponding to that input and find which resource it is bound to, then figure out which position this resource corresponds to at the entrance of the multiplexer, and generate the *select* signal to select that specific input from the multiplexer;
    * if the input is not a multiplexer, there is nothing to do.

Note that when a control signal is not set explicitly to '1', you should set it '0'.
A quick way to achieve this in VHDL is to always set all the control signals to '0' at the beginning of the process that contains your switch statement (once case for each state).
Since in a process, the assignment of signals happens sequentially, it okay to re-assigned some of them to '1' inside each case in the switch statement.


## Block of code

In order to build more interesting programs that contain multiple statements, we will introduce the notion of a block of code.
You will need to add a new type of ASTNode to represent a list of statements:
`Block(stmts: List<Stmt>) extends Stmt`

This will allow you to represents programs such as:
    ```C
    A = malloc(4);
    B = malloc(4);
    B[0] = A[0] * 2;
    B[1] = A[1] * 2;
    B[2] = A[2] * 2;
    B[3] = A[3] * 2;
    ```

Having added this node to your AST, you should update the process that generates the DFG from the AST to deal with the block statement.

Since we do not have any notion of variable at this point, and to keep things simple at this stage, you should assume that the statements in a block are independent from each other (i.e. there are no dependencies between the statements).
Note that you should still generate a single DFG even in the presence of multiple statements in the block (when processing a statement, always add the nodes produced to a single DFG).
Also do not forget that blocks could be nested and all the nodes belonging to a nested block should simply be added to the one parent DFG.

Once a single DFG has been produced, you should be able to proceed as before with your compiler flow without requiring any modification.



## Testing

With all that done, you should test your high-level synthesizer on the following programs.
You should check that you obtain the expected expected time and resource usage for your circuit.

1. Simple test

    ```C
    RAM = malloc(4);
    RAM[3] = RAM[0] * RAM[1] + RAM[2]
    ```
   
   Should take 5 clock cycles with 1 adder and 1 multiplier.

2. Vector multiplication

    ```C
    A = malloc(4);
    B = malloc(4);
    C = malloc(4);
    C[0] = A[0] * B[0];
    C[1] = A[1] * B[1];
    C[2] = A[2] * B[2];
    C[3] = A[3] * B[3];
    ```
    Should take 6 clock cycles with 1 multiplier.

3. Dot product

    ```C
    A = malloc(4);
    B = malloc(4);
    C = malloc(1);
    C[0] = A[0] * B[0] +
           A[1] * B[1] + 
           A[2] * B[2] + 
           A[3] * B[3];
    ```
    Should take 7 clock cycles with 1 multiplier and 1 adder.

4. Convolution

    ```C
    A = malloc(6);
    W = malloc(3);
    C = malloc(4);
    C[0] = A[0] * W[0] + A[1] * W[1] + A[2] * W[2];
    C[1] = A[1] * W[0] + A[2] * W[1] + A[3] * W[2];
    C[2] = A[2] * W[0] + A[3] * W[1] + A[4] * W[2];
    C[3] = A[3] * W[0] + A[4] * W[1] + A[5] * W[2];
    ```
   Should take 15 clock cycles with 1 adder and 1 multiplier. This assumes no optimizations are implemented and, therefore, 24 loads are performed.
