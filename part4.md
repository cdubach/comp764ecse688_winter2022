# Part IV. Extensions/Optimizations

At this stage, you should now have a working compiler that can produce hardware in the form of VHDL code.
We will now see how your compiler can be extended to support additional features and optimizations.

 
## 1. Extensions

## 1.1 Variables and data dependencies

We will first want to add the ability to have variables.
This will be useful to implement loops as we will see shortly as well as other benchmarks.

```Scala
class Var(name: String) extends Expr
```

Having added this AST node in your compiler, you should start with augmenting your backend to deal with this node.


To keep things simple, we will assume that a variable is always stored in a register.
You should treat a variable in a exactly the same way as memory --- after all, a register is a form of memory.
Two operations can be performed on a register; a load or a store.
The main difference compared to the RAM operations is that a load from a register should take 0 clock cycle since the value is available straight-away (as opposed to a load from memory that requires an actual lookup/read operation into the RAM).

One issue that has been so far completely ignored are dependencies between load/store operations.
So far, when using memory, we have assumed that all the load and store address different locations in memory.
However, with a register, all load and store operations occurs at the same location.


Therefore, you will have to take this into account during scheduling (only take this into account for registers, we will still assume load/store to a block RAM are always independent).
We suggest that you augment the DFG with a different type of edge that do not represent data flow, but represent a data dependency.
Obviously any data flow edge is also a data dependency edge.
So if you are using an OO design, one type of edge would be a subclass of the other one.

To illustrate this, the DFG for the following program: 

```C
i = new Register; // similar to a memory allocation, this is not an actual operation
i = 0;
i = i+1;
```

would be:

![alt text](./dots/dfgWithVar.png "data flow graph")

where the dashed edges represent a data dependency, and the solid edges represent data flow.
As far as the scheduler is concerned, both types of edges should be treated in the same way.
On the other hand, when building the graph representing the DataPath, only the solid edges should be taken into consideration.

Since the CST and LDi nodes take 0 cycle to execute, the program above should take 3 clock cycle to execute when no optimizations are considered.



## 1.2 Loops

We will now add supports for loops in order to generate more interesting hardware.
You will need to add a new ASTNode to represents a loop as you can imagine.
To keep things simple, we will simply represent for loops that start at 0, increment by 1 and whose bound is known statically:


```Scala
class For(itVar: Var, itNum: int, body: Stmt) extends Stmt
```

where `itVar` represents the iteration variable (i.e. a register) that can be accessed in the body and `itNum` represents the number of iteration of the loop.

You will now have to properly build a control flow graph with different kind of nodes.
Again, we will try to keep things simple for this project.
Therefore, instead of allowing for arbitrary control flow (i.e. with branching that can jump anywhere), we will restrict ourselves to two types of CFG nodes:

* **OnceNode** which corresponds to a sequence of operations represented by a single DFG that are executed only once
* **ForNode** which corresponds to a sequence of operations represented by a single DFG that are repeated multiple times


You should design a new hardware template for each of these two type of nodes.
For the *For* node, the hardware template's should simply consists of repeating the computation in the DFG until the `itVar` reaches the desired number of iterations.
You can implement the `itVar` simply as a counter and assume that the body of the loop will only load the `itVar` and never modify it.

Given that the number of cycles to execute both type of nodes, and the number of iterations for a `For` node are known statically, it is possible to schedule the control flow graph completely statically.
When designing your template for both type of nodes, simply use a `start` input port that triggers the execution of the node.

## 1.3 Testing

You should now test the extensions above on the following programs.
You will be expected to demonstrate that you can product correct hardware for all these cases during the demo.

1. Simple variable test

    ```C
	i = new Register;
	j = new Register;
	i = 0;
	j = 1;	
	i = i+j;
    ```
   
2. Simple loop test

	```C
	s = new Register;
	s = 0;
	for (i=0..3) { // 4 iterations
		s = s + i;
	}
	
	```

2. Sum of array

    ```C
    A = malloc(4);
	s = new Register;
	s = 0;
	for (i=0..3) { // 4 iterations
		s += A[i]
	}
    ```

3. Dot product

	```C
	vecA = malloc(4);
	vecB = malloc(4);
	s = new Register;
	s = 0;
	for (i=0..3) { // 4 iterations
		s = s+ vecA[i] * vecB[i]; 
	}	
	```



## 2. Optimizations

We will now turn our attention towards implementing some optimizations.
You should at least implement **three** of the optimizations below correctly to score all the points.
For each of them, you will have to demonstrate during the demo, the effect of your optimization on the time it takes to finish the program using the cycle accurate simulator on at least two different examples of your choice.


### 2.1 Common sub-expression elimination (easy)

As seen in the lecture, this optimization consists of identifying identical sub-trees.
You can either implement this optimization at the AST level by inserting variable to save the common sub-expressions or, implement this directly in the DFG.
The convolution example from part 3 would be a great example to use to show the benefit of this optimization.

### 2.2 Chaining (easy)

As seen in the lecture, it is sometimes possible to chain operations that are known to have a very short delay.
You should apply this optimization on a DFG and chain all operations except the Load and Store operations.


### 2.3 Spatial parallelism (medium)

Up until now, the iteration of loops execute one at a time.
Unrolling the body of a loop is likely to expose extra parallelism that can be taken advantage of during scheduling.
You can apply this optimization on the AST or on the CFG/DFG (it is easier to apply on the AST).
The process of unrolling a loop simply consists of duplicating the loop body a certain number of time.
You should ensure that you change the total loop iteration count (divide it by the unrolling factor) and also ensure that you take care of adding an offset to the loop iteration variable.

For instance, the following code:

```C
vecA = malloc(128);
s = new Register;
s = 0;
for (i=0..127) { // 128 iterations
	s = s+ vecA[i]; 
}	
```

would become, after unrolling by a factor 2:

```C
vecA = malloc(128);
s = new Register;
s = 0;
for (i=0..64) { // 64 iterations
	s = s+ vecA[i]; 
	s = s+ vecA[i+1]; 
}	
```




### 2.4 Pipeline parallelism (hard)

As seen in the lecture, another form of parallism is pipeline parallelism.
You can apply this optimization on the AST or on the CFG/DFG (it is easier to apply on the AST).

For instance, the following code:
```C
vecA = malloc(128);
vecB = malloc(128);
vecC = malloc(128);
for (i=0..127) { // 128 iterations
  vecC[i] = vecA[i] + vecB[i]; 
}
```
should be transformed into:
```C
vecA = malloc(128);
vecB = malloc(128);
vecC = malloc(128);
va = new Register;
vb = new Register;
vc = new Register;
vc = vecA[0] + vecV[0];
va = vecA[1];
vb = vecB[1];
for (i=0..125) { // 126 iterations
  vecC[i] = vc;
  vc = va + vb;
  va = vecA[i+2];
  vb = vecB[i+2]; 
}
vecC[126] = vc;
vecC[127] = va + vb;
```

The loop body should only take a single cycle to execute since the load, store and add operations are all independent from each other.
