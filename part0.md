# Part 0 : Setup and learning VHDL

# Setup

### Obtaining the software

For this assignment and your project, we are going to use Intel's Quartus software.
You can download all the software directly from the Intel's website which supports either Windows or Linux (the instructions here are valid for Linux but should be similar for a windows machine). You should choose the Lite version 21.1 and download the *Combined Files* to simplify:

* [Quartus Prime Lite Edition](https://fpgasoftware.intel.com/21.1/?edition=lite&platform=linux).


This contains Quartus, the project manager and synthesiser for Intel's hardware, as well as Questa (formerly known as ModelSim), an HDL simulator.


### Obtaining the license (free)

Next, you should obtain a license (it is free) for Questa as it requires a license to run.

To obtain a license, visit the [Intel FPGA Self Service Licensis Center](https://fpgasupport.intel.com/Licensing/license/index.html) to register an account and then download the license.
The process is a bit painful to say the least, you should in this order:

1. Create a "Computer" under *Computers and License Files* using your NIC as ID (this is the MAC address of the Network interface where you will run the software, i.e. a 12 hexadecimal values). Under license type choose *Fixed*.

2. Then create a license by going to the tab *Sign up for Evaluation or Free Licenses* and get the *Questa*-Intel® FPGA Starter Edition SW* license. Choose "1" seat and assign it to the computer created earlier.

3. Generate the license by following *Licenses* -> *All Licenses*, click on the license (first column) and then click *Generate License*. You will receive an email with your license.

4. Place the license (.dat file) in a folder of your choice in your home directory.


### Installing and running the software

To install the software, simply unpack the file you have downloaded earlier, and run the setup (setup.sh on Linux) and choose most of the default options.

Next, launch Quartus from the command line using from the folder where you installed Quartus, for instance:

`LM_LICENSE_FILE=PATH_TO_LICENSE_FILE ~/intelQuartus/21.1/quartus/bin/quartus`

You should the graphical interace launching.

We are next going to check that the simulator is properly setup with the license file. While the simulator can be directly invoked from Quartus, we are going to check frmo the command line that everything works.
Run the following command and check that the simulator graphical interface opens up.

`LM_LICENSE_FILE=PATH_TO_LICENSE_FILE ~/intelQuartus/21.1/questa_fse/linux_x86_64/vsim`

If you do not see the grapgical interface launching, it means that your license is not setup properly and you should check the path to your license, or any error message on the command line to help debug the issue.


### Asking for help

If you are unable to setup the software properly, please ask on ED for help. Please do include screenshots as this might help find the issue.


## Learning to use the tools

To learn how to use the tools, we suggest that you follow the [Getting Started with Quartus CAD Software tutorial](./ECSE_222_Fall2021_VHDL_1.pdf) borrowed from the ECSE-222 course.
Since the version of Quartus is older in this tutorial, you will have to adapt a couple of things but hopefully it should not be too different (in case of problems, ask on ED).




## Learning VHDL

If you are new to VHDL, or need to refresh your skills, we recommend that you follow this excellent [FPGA designs with VHDL tutorial](https://vhdlguide.readthedocs.io/en/latest/index.html).

We suggest the following order when doing the tutorial.

### 1.First project

Make sure that you are able complete this part of the tutorial, without actually re-installing the software.

Also since we are not going to use a real FPGA, you should skip the part *1.5 Load the design on FPGA*.

### 10. Testbenches

Having seen how to create a project with a simple hardware, we suggest that you look at how to test your design. For this, follow sections 10.1 and 10.2 of the tutorial (you will com eback to the 10.3 later).

### 2. to 6.

Next follow the tutorial sections 2. to 6. inclusive.

### 9. Finite state machines

You should then read about how finite sate machines are encoded in VHDL.
Pay particular attention to Moore's deign for FSM as this is what you should always use to avoid timing issues as much as posssible and produce good design.
Mealy machines should only be use in very specific situation and we suggest that you do not use them in your designs.

### 10.3 Testbench for sequential circuits

Finally, you should follow the section 10.3 which decribes how to test sequential circuits.

If you have survived so far, congratulations! You should now know how to use VHDL for desiging hardware.


